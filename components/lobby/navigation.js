$(document).ready(() => {
  $('.category > .category__tv').addClass('active');
  let focus, index, total, link;

  const Move = {
    Up: 38,
    Down: 40,
    Left: 37,
    Right: 39
  };

  $(document).keydown((event) => {
    focus = $('.active');
    console.log('focus', focus);
    index = focus.index();
    total = focus.parent().children().length;
    link = focus.find('img').attr('longdesc');
    switch (event.keyCode) {
      case Move.Left:
        if(index < 1) return;
        focus.removeClass('active').prev().addClass('active');
        if (index < total - (6 - 1)) {
          focus.parent().css('transform', 'translateX(-' + (0) + 'px)');
        }
        break;
      case Move.Right:
        if(index > 10) return;
        focus.removeClass('active').next().addClass('active');
        if (index > 4) {
          focus.parent().css('transform', 'translateX(-' + ((index - 4) * 510) + 'px)');
        }
        break;
      case 13:
        window.location.href = link;
    }
  });
});
