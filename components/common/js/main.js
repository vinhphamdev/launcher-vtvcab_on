$(document).ready(function() {
    var width = window.innerWidth;
    var height = window.innerHeight;
    var newWidth = height / 9 * 16;
    var scaleX = newWidth / 3840;
    var scaleY = height / 2160;
    $("#app-container").css("transform", "scale(" + scaleX + "," + scaleY + ")");
    $("#app-container").css("margin-left", (width - newWidth) / 2);
});