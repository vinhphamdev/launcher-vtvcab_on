$(document).ready(() => {
  const Move = {
    Up: 38,
    Down: 40,
    Left: 37,
    Right: 39
  };

  const NumberOfDisplayElement = {
    TopMenu: 8,
    TopSubMenu: 7,
    Movies: 6
  };

  function showHide(parent, action) {
    if (parent.parents('.same-level').hasClass('row1')) {
      action === true ? $('.prev1').show() : $('.prev1').hide();
    } else if (parent.parents('.same-level').hasClass('row2')) {
      action === true ? $('.prev2').show() : $('.prev2').hide();
    } else if (parent.parents('.same-level').hasClass('row3')) {
      action === true ? $('.prev3').show() : $('.prev3').hide();
    }
  }

  function scroll(parent, cal = true) {
    const position = parent.scrollLeft();
    const blockElem = cal === true ? 390 : -390;
    parent.stop().animate({scrollLeft: position + blockElem}, 250);
  }

  let focus, parent, index, total, layout;
  // set active to first item
  $('.top-menu > button:first-child').addClass('active');

  // hide button previous at first position
  $('.btn-left').hide();
  $('.top-sub-menu__btn-left').hide();

  $('.prev1').hide();
  $('.prev2').hide();
  $('.prev3').hide();

  // event binding move up, right, down, left
  $(document).keydown((event) => {
    focus = $('.active');
    parent = focus.parent();
    total = parent.children().length;
    index = $('.active', parent).index();
    layout = $(focus.parents('.same-level'));
    switch (event.keyCode) {
      case Move.Left:
        // can't previous at first element
        if (index === 0) return;
        focus.removeClass('active').prev().addClass('active');

        // top-menu
        if (parent.hasClass('top-menu')) {
          if (index === 1) $('.btn-left').hide();
          if (index < total - (NumberOfDisplayElement.TopMenu - 1)) {
            $('.btn-right').show();
            scroll(parent, false);
          }
        }

        // top-sub-menu
        else if (parent.hasClass('top-sub-menu')) {
          if (index === 1) $('.top-sub-menu__btn-left').hide();
          if (index < total - (NumberOfDisplayElement.TopSubMenu - 1)) {
            $('.top-sub-menu__btn-right').show();
            scroll(parent, false);
          }
        }

        // category
        else if (parent.hasClass('row__inner')) {
          if (index === 1) {
            showHide(parent, false);
          }
          if (index < total - (NumberOfDisplayElement.Movies - 1)) {
            parent.css('transform', 'translateX(-' + ((index - 1) * 700) + 'px)');
          }
        }
        break;
      case Move.Up:
        if (parent.hasClass('top-menu')) return;

        focus.removeClass('active');

        // top-menu đang ở điểm cuối
        // top-sub-menu -> top-menu. top-menu sẽ scrollLeft về đầu
        if (layout.prev().hasClass('top-menu')) {
          layout.prev().scrollLeft(0);
          $('.btn-right').show();
        } else {
          layout.prev().find('.row__inner').css('transform', 'translateX(0)');
        }

        if (layout.prev().length) {
          layout.prev().find('.cc').addClass('active');
        }
        break;
      case Move.Right:
        // can't next at last element
        if (total - index === 1) return;

        focus.removeClass('active').next().addClass('active');

        // top-menu
        if (parent.hasClass('top-menu')) {
          if (total - index === 2) $('.btn-right').hide();
          if (index > NumberOfDisplayElement.TopMenu - 2) {
            $('.btn-left').show();
            scroll(parent);
          }
        }

        // top-sub-menu
        else if (parent.hasClass('top-sub-menu')) {
          if (total - index === 2) $('.top-sub-menu__btn-right').hide();
          if (index > NumberOfDisplayElement.TopSubMenu - 2) {
            $('.top-sub-menu__btn-left').show();
            scroll(parent);
          }
        }

        // category
        else if (parent.hasClass('row__inner')) {
          if (index > NumberOfDisplayElement.Movies - 2) {
            showHide(parent, true);
            parent.css('transform', 'translateX(-' + ((index - NumberOfDisplayElement.Movies + 2) * 700) + 'px)');
          }
        }
        break;
      case Move.Down:
        focus.removeClass('active');

        if (layout.next().hasClass('top-sub-menu')) {
          layout.next().scrollLeft(0);
          $('.top-sub-menu__btn-right').show();
        } else {
          layout.next().find('.row__inner').css('transform', 'translateX(0)');
        }

        if (layout.next().length) {
          layout.next().find('.cc').addClass('active');
        }
        break;
    }
  });
});
