$(document).ready(() => {
  let focus, index, parent;
  $('.form-sign-up__word > li:first-child').addClass('active');
  const Move = {
    Up: 38,
    Down: 40,
    Left: 37,
    Right: 39
  };

  $(document).keydown((event) => {
    focus = $('.active');
    index = focus.index();
    parent = focus.parent();
    switch (event.keyCode) {
      case Move.Left:
        if (index < 1) return;
        focus.removeClass('active').prev().addClass('active');
        break;
      case Move.Right:
        focus.removeClass('active').next().addClass('active');
        break;
      case Move.Down:
        focus.removeClass('active');
        if (parent.next().length) {
          parent.next().find('.cc').addClass('active');
        }
        break;
      case Move.Up:
        focus.removeClass('active');
        if (parent.prev().length) {
          parent.prev().find('.cc').addClass('active');
        }
    }
  });
});
